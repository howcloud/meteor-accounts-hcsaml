var SamlTokens = new Meteor.Collection("samlTokens"); // we store tokens => login data in here
var SamlIdentityProviders = new Meteor.Collection("samlIdentityProviders");

/*

Requires: options.samlToken
		  options.mode = login, link or new

*/

var _linkSamlService = function (idp, authData, _user_id) {
	var set = {};
	set['services.'+idp] = authData;
 
	Meteor.users.update(_user_id, {
		$set: set,
		$unset: {
			'guest': '',
			'profile.guest': ''
		}
	});
}

var _invalidLoginModeError = function () {
	return { 
		type: "samlToken",
		error: new Meteor.Error(Accounts.LoginCancelledError.numericError, "Invalid login mode") 
	}
}

Accounts.registerLoginHandler("samlToken", function (options) {
	if (!options.samlToken) return undefined; // don't handle
	
	var token = SamlTokens.findOne({token: options.samlToken, used: {$exists: false}});
	if (!token || !options.mode) {
		return { 
			type: "samlToken",
			error: new Meteor.Error(Accounts.LoginCancelledError.numericError, "Invalid saml token") 
		}
	} else {
		// TODO: Lots of this could probably be refactored

		// loginData:
		// what this function returns to deal with the login
		var loginData = null;

		// Auth data 
		var _authData = _.clone(token.authData);
		_authData.type = 'saml';

		// idp info
		var idp = SamlIdentityProviders.findOne({entityId: token.entityId});
		var _service = idp.codename ? idp.codename : idp.name;

		var thisUser = Meteor.user();
		var samlUser = Meteor.users.getServiceUser(_service, _authData);

		// no GUARANTEE that the assertion was done in hcsaml_tokenLoginOptions method, so we do it again here - worst case, doesn't hurt
		var samlAccount = ServiceAccounts.assertServiceAccount(_service, token.authData); // assert => one will always exist or be created, we use the original authData from the token here, rather than our mutated version
		samlAccount.assertAttributes(token.friendlyAttributes); // this asserts profile information which will be applied to the user linked to this service account
		samlAccount = ServiceAccounts.getServiceAccount(_service, token.authData); // HACKY TEMP THING: quicker (coding wise!) to just get again once the assertion is done rather than guarantee an in place edit on the assertAttributes function (this is a todo though)

		if (options.mode == 'login') { // login straight away into the saml account
			if (samlUser) {
				loginData = Accounts.updateOrCreateUserFromExternalService(_service, _authData, {});
				samlAccount.applyToUser(loginData.userId); // note this means we RE APPLY, must guarantee that this doesn't error out/double up data
			} else {
				return _invalidLoginModeError();
			}
		} else if (options.mode == 'link') { // link the saml account to the current user
			if (samlUser || !thisUser) return _invalidLoginModeError(); // if there already exists a linked user or we do not have an account to link this user to, can't do this
			
			_linkSamlService(_service, token.authData, thisUser._id);
			samlAccount.applyToUser(thisUser._id);

			loginData = Accounts.updateOrCreateUserFromExternalService(_service, _authData, {});
		} else if (options.mode == 'new') {
			if (samlUser) return _invalidLoginModeError();

			if (samlAccount.needsName() && !options.samlName) return _invalidLoginModeError();
			if (samlAccount.needsEmail() && !options.samlEmail) return _invalidLoginModeError();

			if (!thisUser || (thisUser && !thisUser.isGuest())) { // there is a logged in user but we want to force a new account anyway
				loginData = Accounts.updateOrCreateUserFromExternalService(_service, _authData, {_FORCE_NEW_ACCOUNT: true});
				samlAccount.applyToUser(loginData.userId);
			} else if (thisUser && thisUser.isGuest()) { // there is a logged in GUEST => a new account is just linking up this current guest account
				_linkSamlService(_service, token.authData, thisUser._id);
				samlAccount.applyToUser(thisUser._id);

				loginData = Accounts.updateOrCreateUserFromExternalService(_service, _authData, {});
			} else {
				return _invalidLoginModeError(); // dunno if we can ever reach this point...
			}

			if (samlAccount.needsName() || samlAccount.needsEmail()) {
				if (loginData) {
					if (samlAccount.needsEmail()) {
						var user = Meteor.users.getById(loginData.userId);
						user.pushEmail(options.samlEmail);
					}

					if (samlAccount.needsName()) Meteor.users.update(loginData.userId, {'$set': {'profile.name': options.samlName}});
				}
			}
		}

		if (!loginData) return _invalidLoginModeError(); // this shouldn't ever be reached

		// SamlTokens.remove({token: options.samlToken}); // these are one use only, if we got this far then we are 'used' the token
		// we no longer want to remove these, just set them as 'used', useful for debugging implementations to store the data
		SamlTokens.update({token: options.samlToken}, {$set: {used: true}});

		return loginData;
	}
});

Accounts.addAutopublishFields({
	forLoggedInUser: ['services.saml']
});

/** SAML Token Login Summary **/
// Returns info on the token that the user wants to login with as well as specifics about the actions availble.

Meteor.methods({
	_hcsaml_tokenLoginOptions: function (_token) {
		var token = SamlTokens.findOne({token: _token, used: {$exists: false}});
		if (!token) throw new Meteor.Error(404, "SAML token not found");

		var thisUser = Meteor.user();
		if (thisUser && thisUser.isGuest()) thisUser = null; // for the sake of this function being a guest is the same as just allowing us to go straight to login
															 // in reality (see login method above) we actually do a link to the user's account

		// idp info
		var idp = SamlIdentityProviders.findOne({entityId: token.entityId});
		var _service = idp.codename ? idp.codename : idp.name;

		// get user associated with this saml login attempt, if one exists
		var samlUser = Meteor.users.getServiceUser(_service, token.authData);

		// assert a samlAccount in virtue of this login attempt and assert attributes so we know if we need to gather anymore information from them before it should be applied to a user
		var samlAccount = ServiceAccounts.assertServiceAccount(_service, token.authData);
		samlAccount.assertAttributes(token.friendlyAttributes); 
		samlAccount = ServiceAccounts.getServiceAccount(_service, token.authData); 

		if (samlUser) { // if there is an account linked to this saml account already - login
			return {
				idpName: idp.name,
				options: ['login']
			}
		} else if (!thisUser) { // if the user is not logged into an account - can create an account/log them in
			return {
				idpName: idp.name,
				options: ['new'],

				requireName: samlAccount.needsName(),
				requireEmail: samlAccount.needsEmail(),
			}
		} else {
			return {
				idpName: idp.name,
				options: ['link', 'new'],

				requireName: samlAccount.needsName(),
				requireEmail: samlAccount.needsEmail(),
			}
		}
	}
});