Package.describe({
	name: 'howcloud:accounts-saml',
	summary: "Integrates HowCloud SAML authentication workflow with the Meteor accounts system"
});

Package.on_use(function (api) {
	
	/* Package Dependencies */

	api.use('underscore');
	api.use('ddp');
	api.use('mongo');
	api.use('accounts-base');

	/* Add files */

	api.add_files('hcsaml_shared.js');
	api.add_files('hcsaml_client.js', 'client');
	api.add_files('hcsaml_server.js', 'server');

});