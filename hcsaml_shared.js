_samlUrl = function (_idp, _continue) {
	var url = HCConfig.auth_url+'saml/_platAuth.php?app='+HCConfig.instance;
	url += '&idp='+_idp;

	if (_continue) url += '&continue='+encodeURIComponent(_continue);

	return url;
}

Meteor.loginWithSamlUrl = _samlUrl;