Meteor.loginWithSaml = function(idp, options) {
	var url = _samlUrl(idp, options ? options.continueUrl : null);	
	window.location = url;
};

Meteor.loginWithSamlToken = function (_mode, _data, callback) {
	_data = _data || {};

	Accounts.callLoginMethod({
		methodArguments: [_.extend({mode: _mode}, _data)],
		userCallback: callback
	});
}